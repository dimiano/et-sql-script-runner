﻿using System;

namespace SqlScriptsRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;

            Console.WindowWidth = 150;
            Console.SetBufferSize(150, 150);
            Console.WriteLine();

            var scriptRunner = new ScriptRunner();
            bool successed = scriptRunner.ProcessScripts(args);// new[] { @"D:\inetpubs" });
            if (successed)
            {
                scriptRunner.BeepComplete(); 
            }

            //Pause();
        }
        private static void Pause()
        {
            Console.WriteLine("\nPress any key to continue . . .");
            Console.ReadKey(true);
        }

        private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ScriptRunner.WriteLine(string.Format("\nUnhandled Error:\n{0}\n", e.ExceptionObject), ConsoleColor.Red);
        }
    }
}
