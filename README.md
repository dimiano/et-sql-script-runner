### ET SQL Scripts Runner is a simple console tool to apply SQL scripts to database. 

Its required sqlcmd tool (installing within MS SQL Server).
 

### Command line args:

*  **?** - help
*  **[any other string]** - directory path, with SQL scripts included, to progess
*  **[no parameters]** - recursive process "D:\inetpub\Database\" directory if exists
*  **reset** - reset the app settings to default values